package com.company.Components;

public enum Color {
    Yellow,
    Red,
    Blue,
    Orange,
    Black,
    White
}
