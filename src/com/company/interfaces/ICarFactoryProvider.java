package com.company.interfaces;

import com.company.CarBuild;

import java.io.IOException;

public interface ICarFactoryProvider {
    CarBuild getCarFactory(String model) throws IOException;
}
