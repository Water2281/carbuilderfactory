package com.company;

import com.company.Components.CarType;
import com.company.Components.Color;
import com.company.Components.Control;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class CarParametrSettings {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    CarType carType; Color color; Control controller;

    public ArrayList getCarParametrs() throws IOException {
        System.out.println("Enter type(Luxury,Small,Sedan)");
        String type = null;
        type = reader.readLine();
        System.out.println("Enter color(Yellow,White,Orange,Black,Red,Blue)");
        String c = null;
        c = reader.readLine();
        System.out.println("Enter engine volume(1, 2, 3)");
        String engineVol = null;
        engineVol = reader.readLine();
        System.out.println("Enter control type(Auto,Mechanic)");
        String control = null;
        control = reader.readLine();
        ArrayList a = new ArrayList<>();
        a.add(carType.valueOf(type));  a.add(color.valueOf(c)); a.add(controller.valueOf(control)); a.add(engineVol);
        return a;
    }
}
