package com.company;

import com.company.Components.CarType;
import com.company.Components.Color;
import com.company.Components.Control;
import com.company.Components.Model;

public abstract class Car {
     protected Model model;
     protected CarType type;
     protected Color color;
     protected int engineVol;
     protected Control control;

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getEngineVol() {
        return engineVol;
    }

    public void setEngineVol(int engineVol) {
        this.engineVol = engineVol;
    }

    public CarType getType() {
        return type;
    }

    public void setType(CarType type) {
        this.type = type;
    }

    public Control getControl() {
        return control;
    }

    public void setControl(Control control) {
        this.control = control;
    }

}
