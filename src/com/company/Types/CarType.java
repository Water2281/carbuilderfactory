package com.company.Types;

public enum CarType {
    Small,
    Luxury,
    Sedan,
}
