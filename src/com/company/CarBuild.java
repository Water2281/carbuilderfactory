package com.company;

import com.company.Components.CarType;
import com.company.Components.Color;
import com.company.Components.Control;
import com.company.Components.Model;
import com.company.interfaces.CarFactory;

public class CarBuild extends Car implements CarFactory {

    public CarBuild(CarBuilder carBuilder) {
        super.model = carBuilder.getModel();
        super.type = carBuilder.getType();
        super.color = carBuilder.getColor();
        super.engineVol = carBuilder.getEngineVol();
        super.control = carBuilder.getControl();
    }

    @Override
    public void create() {
            System.out.println(super.model + " has been created with parameters:" + super.type + ", " + super.color + ", "
                    + super.control + ", " + super.engineVol);
    }

    public static class CarBuilder extends Car{

        public CarBuilder(Model model) {
            super.model = model;
        }

        public CarBuilder Type(CarType type){
            super.type = type;
            return this;
        }

        public CarBuilder Color(Color c){
            super.color = c;
            return this;
        }

        public CarBuilder EngineVolume(int e){
            super.engineVol = e;
            return this;
        }

        public CarBuilder Control(Control c){
            super.control = c;
            return this;
        }

        public CarBuild build(){
            return new CarBuild(this);
        }

    }

}