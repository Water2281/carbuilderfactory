package com.company;

import com.company.Components.CarType;
import com.company.Components.Color;
import com.company.Components.Control;
import com.company.Components.Model;
import com.company.Factories.KiaFactory;
import com.company.Factories.SubaruFactory;
import com.company.Factories.ToyotaFactory;
import com.company.interfaces.ICarFactoryProvider;

import java.io.IOException;
import java.util.ArrayList;


public class CarFactoryProvider implements ICarFactoryProvider {
    CarParametrSettings cr = new CarParametrSettings();

    public CarBuild getCarFactory(String model) throws IOException {
        if(model.equals("Kia")){
            ArrayList parametrs = cr.getCarParametrs();
            return new KiaFactory
                    .CarBuilder(Model.Kia)
                    .Type((CarType) parametrs.get(0))
                    .Color((Color) parametrs.get(1))
                    .Control((Control) parametrs.get(2))
                    .EngineVolume((Integer.parseInt((String) parametrs.get(3))))
                    .build();
        }
        if(model.equals("Toyota")){
            ArrayList parametrs = cr.getCarParametrs();
            return  new ToyotaFactory
                    .CarBuilder(Model.Toyota)
                    .Type((CarType) parametrs.get(0))
                    .Color((Color) parametrs.get(1))
                    .Control((Control) parametrs.get(2))
                    .EngineVolume((Integer.parseInt((String) parametrs.get(3))))
                    .build();
        }
        if(model.equals("Subaru")){
            ArrayList parametrs = cr.getCarParametrs();
            return  new SubaruFactory
                    .CarBuilder(Model.Subaru)
                    .Type((CarType) parametrs.get(0))
                    .Color((Color) parametrs.get(1))
                    .Control((Control) parametrs.get(2))
                    .EngineVolume((Integer.parseInt((String) parametrs.get(3))))
                    .build();
        }
        return null;
    }

}
