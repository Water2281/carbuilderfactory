package com.company;

import com.company.Factories.KiaFactory;
import com.company.interfaces.CarFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        CarFactoryProvider carFactoryProvider = new CarFactoryProvider();
        System.out.println("Enter your model of car(Kia,Toyota,Subaru)");
        String model = null;
        model = reader.readLine();
        CarBuild car = carFactoryProvider.getCarFactory(model);
        car.create();
    }
}
