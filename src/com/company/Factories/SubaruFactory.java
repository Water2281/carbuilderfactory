package com.company.Factories;

import com.company.CarBuild;
import com.company.Components.Model;


public class SubaruFactory extends CarBuild {
    public SubaruFactory(CarBuilder carBuilder) {
        super(carBuilder);
        setModel(Model.Subaru);
    }

    @Override
    public void create() {
        System.out.println("Model Subaru has been created with parameters:" +  super.type + ", " + super.color + ", "
                + super.control + ", " + super.engineVol);
    }
}
