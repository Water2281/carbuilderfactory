package com.company.Factories;

import com.company.CarBuild;
import com.company.Components.Model;

public class ToyotaFactory extends CarBuild {
    public ToyotaFactory(CarBuilder carBuilder) {
        super(carBuilder);
        setModel(Model.Toyota);
    }

    @Override
    public void create() {
        System.out.println("Model Toyota has been created with parameters:" +  super.type + ", " + super.color + ", "
                + super.control + ", " + super.engineVol);
    }
}
