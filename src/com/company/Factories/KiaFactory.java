package com.company.Factories;

import com.company.CarBuild;
import com.company.Components.Model;

public class KiaFactory extends CarBuild{
    public KiaFactory(CarBuilder carBuilder) {
        super(carBuilder);
        super.model = Model.Kia;
    }

    @Override
    public void create() {
        System.out.println("Model Kia has been created with parameters:" +  super.type + ", " + super.color + ", "
                + super.control + ", " + super.engineVol);
    }
}
